var createError = require('http-errors');
var express = require('express');
var mongoose = require('mongoose');
var cors = require('cors')
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');

const db = require('./config/database'); //database configuration

// connection to mongodb
db.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

var studentRouter = require('./routes/students');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('secretKey', 'nodeRestApi'); // jwt secret token
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/student', validateUser, studentRouter);
app.use('/users', usersRouter);

//jwt verification
function validateUser(req, res, next) {
  jwt.verify(req.headers['x-api-key'], req.app.get('secretKey'), function(err, decoded) {
    if (err) {
      res.json({status:"error", message: err.message, data:null});
    }else{
      // add user id to request for verification
      req.body.userId = decoded.id;
      next();
    }
  });
  
}

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// handle 404 error
app.use(function(req, res, next) {
 let err = new Error('Not Found');
    err.status = 404;
    next(err);
});
// handle errors
app.use(function(err, req, res, next) {
 console.log(err);
  if(err.status === 404)
   res.status(404).json({message: "Not found"});
  else 
    res.status(500).json({message: "Something looks wrong :( !!!"});
});

module.exports = app;
