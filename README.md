**Node Express CRUD operations on Students.**

*Everything is running on port 3000.*

* Script - NodeJS Version 9.10.1
* Database - MongoDB Version 3.6.3
* Testing Tool - Postman


**Steps to run :- **
1. Download the repo(ignore the node_modules folder mistakenly push it or you can delete it after download)
2. Open Terminal and go to project directory.
3. Install Dependencies using "npm install" command.
4. Start the Server using "npm start" command.
5. check the logs on terminal.


Thanks.

