//Set up mongoose connection
const mongoose = require('mongoose');
const mongoDB = 'mongodb://localhost/node-mongo-login';
mongoose.connect(mongoDB,{ useNewUrlParser: true, useUnifiedTopology: true});
mongoose.set('useFindAndModify', false);
mongoose.Promise = global.Promise;
module.exports = mongoose;