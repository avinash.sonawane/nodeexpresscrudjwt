const userModel = require('../models/users');
const bcrypt = require('bcrypt'); 
const jwt = require('jsonwebtoken');
module.exports = {
create: function(req, res, next) {
    let name = (req.body.name) ? req.body.name : "";
    let email = (req.body.email) ? req.body.email : "";
    let password = req.body.password ? req.body.password : "";
    let createdAt = new Date();

    var userObj = {
      "name": name,
      "email" : email,
      "password" : password,
      "createdAt" : createdAt
    };

    userModel.create(userObj, function (err, result) {
      if (err) 
        next(err);
      else
        res.json({status: "success", message: "User added successfully!!!", data: null});
    });
   },
login: function(req, res, next) {
  userModel.findOne({email:req.body.email}, function(err, userInfo){
    if (err) {
      res.json({status:"error", message: "Database error!!!", data:null});
    } else if(userInfo == null){
      res.json({status:"error", message: "Invalid email/password!!!", data:null});
    } else {
      if(bcrypt.compareSync(req.body.password, userInfo.password)) {
        const token = jwt.sign({id: userInfo._id}, req.app.get('secretKey'), { expiresIn: '1h' });
        res.json({status:"success", message: "user found!!!", data:{user: userInfo, token:token}});
      }else{
        res.json({status:"error", message: "Invalid email/password!!!", data:null});
      }
    }
  });
 },
}