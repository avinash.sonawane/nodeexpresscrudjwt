const studentModel = require('../models/students');
const paginate = require('jw-paginate');

module.exports = {
  getById: function(req, res, next) {
    studentModel.findById(req.params.studId, function(err, studInfo){
      if (err) {
        next(err);
      } else {
        res.json({status:"success", message: "Student found!!!", data:{student: studInfo}});
      }
    });
  },
  getAll: function(req, res, next) {
    let studentList = [];
    // get page from query params or default to first page
    let page = parseInt(req.query.page) || 1;
    // get pager object for specified page
    let pager;
    // get page of items from items array
    let pageOfItems;
    studentModel.find({}, function(err, students){
      if (err){
        next(err);
      } else{
        pager = paginate(students.length, page);
        for (let student of students) {
          studentList.push({id: student._id,rollno: student.rollno, name: student.name, branch: student.branch, year: student.year, email: student.email, phone: student.phone,gender: student.gender,age: student.age,city: student.city});
        }
        pageOfItems = students.slice(pager.startIndex, pager.endIndex + 1);
        // return pager object and current page of items
        return res.json({ pager, pageOfItems });
        //res.json({status:"success", message: "Student list found!!!", data:{students: studentList}});
      }
    });
  },
  updateById: function(req, res, next) {
    let sName = (req.body.name) ? req.body.name : "";
    let semail = (req.body.email) ? req.body.email : "";
    let sPhone = (req.body.phone) ? req.body.phone : "";
    let sGender = (req.body.gender) ? req.body.gender : "";
    let sAge = (req.body.age) ? req.body.age : "18";
    let sCity = (req.body.city) ? req.body.city : "";
    let sRoll = (req.body.rollno) ? req.body.rollno : "***";
    let sBranch = (req.body.branch) ? req.body.branch : "Engineering";
    let sYear = (req.body.year) ? req.body.year : "";
    let sUserid = (req.body.userId) ? req.body.userId : "";
    let lastModified = new Date();

    let studObj = {
      "name": sName,
      "email" : semail,
      "phone" : sPhone,
      "gender" : sGender,
      "age" : sAge,
      "city" : sCity,
      "rollno" : sRoll,
      "branch" : sBranch,
      "year" : sYear,
      "userId" : sUserid,
      "lastModified" : lastModified
    };
    studentModel.findOneAndUpdate({"_id" : req.params.studId}, studObj, {upsert: true}, function(err, studentInfo){
      if(err)
        next(err);
      else {
        res.json({status:"success", message: "Student updated successfully!!!", data:null});
      }
    });
  },
  deleteById: function(req, res, next) {
    studentModel.findByIdAndRemove(req.params.studId, function(err, studentInfo){
      if(err)
        next(err);
      else {
        res.json({status:"success", message: "Student deleted successfully!!!", data:null});
      }
    });
  },
  create: function(req, res, next) {
    let sName = (req.body.name) ? req.body.name : "";
    let semail = (req.body.email) ? req.body.email : "";
    let sPhone = (req.body.phone) ? req.body.phone : "";
    let sGender = (req.body.gender) ? req.body.gender : "";
    let sAge = (req.body.age) ? req.body.age : "18";
    let sCity = (req.body.city) ? req.body.city : "";
    let sRoll = (req.body.rollno) ? req.body.rollno : "***";
    let sBranch = (req.body.branch) ? req.body.branch : "Engineering";
    let sYear = (req.body.year) ? req.body.year : "";
    let sUserid = (req.body.userId) ? req.body.userId : "";
    let createdAt = new Date();
    let lastModified = new Date();

    var studObj = {
      "name": sName,
      "email" : semail,
      "phone" : sPhone,
      "gender" : sGender,
      "age" : sAge,
      "city" : sCity,
      "rollno" : sRoll,
      "branch" : sBranch,
      "year" : sYear,
      "userId" : sUserid,
      "createdAt" : createdAt,
      "lastModified" : lastModified
    };

    studentModel.create(studObj, function (err, result) {
      if (err) 
        next(err);
      else
        res.json({status: "success", message: "Student added successfully!!!", data: null});
    });
  },
}