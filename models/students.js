const mongoose = require('mongoose');
//Define a schema
const Schema = mongoose.Schema;

const StudentSchema = new Schema({
	name: {type: String,trim: true,required: true},
	email: {type: String,trim: true,required: true},
	phone: {type: String,trim: true,required: true},
	gender: {type: String,trim: true},
	age: {type: String,trim: true},
	city: {type: String,trim: true},
	rollno : {type: String,trim: true,required: true},
	branch: {type: String,trim: true,required: true,enum:['computer','civil','mechanical','electrical','automobile']},
	year: {type: String,trim: true,required: true,enum:["1","2","3","4"]},
	userId : Schema.ObjectId,
	createdAt: {type: Date,trim: true, default:Date.now},
	lastModified: {type: Date,trim: true, default:Date.now},
});
module.exports = mongoose.model('Student', StudentSchema)