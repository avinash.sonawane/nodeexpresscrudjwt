const express = require('express');
const router = express.Router();
const StudentController = require('../controllers/students');

router.get('/', StudentController.getAll);
router.post('/', StudentController.create);
router.get('/:studId', StudentController.getById);
router.put('/:studId', StudentController.updateById);
router.delete('/:studId', StudentController.deleteById);
module.exports = router;